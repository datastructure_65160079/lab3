package lab3;
public class SearchInRotatedSortedArray {
    public static void main(String[] args) {
        int[] nums = {4,5,6,7,0,1,2};
        int out = 0;
        int target = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == target) {
                out = i;
                break;
            } else {
                out = -1;
            }
        }
        System.out.println(out);
    }
}